﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace MR {
    [RequireComponent(typeof(Canvas))]
    public class MRGUI : MonoBehaviour {
        public GameObject captureMode;
        public GameObject calibrationMode;
        public GameObject recordingMode;
        public GameObject sharedUi;

        SteamVR_TrackedController leftController;
        SteamVR_TrackedController rightController;

        bool hideAllUi; // should all user interface elements be hidden?
        bool controllersSwapped; // are left and right controllers swapped?

        const string controllersSwappedPref = "MR.ControllersSwapped";

        bool controllersVisible = true;

        void Awake() {
            SteamVR_ControllerManager controllerManager = FindObjectOfType<SteamVR_ControllerManager>();
            if (controllerManager == null)
            {
                Debug.LogError("Please add the SteamVR Camera Rig prefab to the scene!");
                return;
            }
            GameObject leftControllerGo = controllerManager.left;
            GameObject rightControllerGo = controllerManager.right;
            leftController = leftControllerGo.GetComponent<SteamVR_TrackedController>();
            rightController = rightControllerGo.GetComponent<SteamVR_TrackedController>();
            if (leftController == null) {
                leftController = leftControllerGo.AddComponent<SteamVR_TrackedController>();
            }
            if (rightController == null)
            {
                rightController = rightControllerGo.AddComponent<SteamVR_TrackedController>();
            }

            controllersSwapped = PlayerPrefs.GetInt(controllersSwappedPref) > 0;
            AssignControllers();
            SetControllerVisible(true);
        }

        void Update() {
            bool showCapture = MixedReality.Instance.mode == MixedReality.Mode.Capture;
            bool showCalibration = MixedReality.Instance.mode == MixedReality.Mode.Calibration;
            bool showRecording = MixedReality.Instance.mode == MixedReality.Mode.Recording;
            captureMode.SetActive(showCapture && !hideAllUi);
            calibrationMode.SetActive(showCalibration && !hideAllUi);
            recordingMode.SetActive(showRecording && !hideAllUi);
            sharedUi.SetActive(!hideAllUi);

            if (Input.GetKeyDown(KeyCode.Alpha1)) {
                SelectCaptureMode();
            } else if (Input.GetKeyDown(KeyCode.Alpha2)) {
                SelectCalibrationMode();
            } else if (Input.GetKeyDown(KeyCode.Alpha3)) {
                SelectRecordingMode();
            } else if (Input.GetKeyDown(KeyCode.F1)) {
                ToggleGUI();
            } else if (Input.GetKeyDown(KeyCode.S)) {
                SwapControllers();
            }

            if (Input.GetKeyDown(KeyCode.Period)) {
                SetControllerVisible(!controllersVisible);
            }

            AssignControllers();
        }

        public void SetControllerVisible(bool visible) {
            Debug.Log("Setting controller visibility to: " + visible);
            GameObject leftModel = leftController.transform.FindChild("Model").gameObject;
            GameObject rightModel = rightController.transform.FindChild("Model").gameObject;

            leftModel.SetActive(visible);
            rightModel.SetActive(visible);
            controllersVisible = visible;
        }

        void AssignControllers() {
            if (controllersSwapped) {
                MixedReality.Instance.camController = rightController;
                MixedReality.Instance.wandController = leftController;
            } else {
                MixedReality.Instance.camController = leftController;
                MixedReality.Instance.wandController = rightController;
            }
        }

        public void SelectCaptureMode() {
            MixedReality.Instance.mode = MixedReality.Mode.Capture;
        }

        public void SelectCalibrationMode() {
            MixedReality.Instance.mode = MixedReality.Mode.Calibration;
        }

        public void SelectRecordingMode() {
            MixedReality.Instance.mode = MixedReality.Mode.Recording;
        }

        public void ToggleGUI() {
            hideAllUi = !hideAllUi;
        }

        public void SwapControllers() {
            controllersSwapped = !controllersSwapped;
            PlayerPrefs.SetInt(controllersSwappedPref, controllersSwapped ? 1 : 0);
        }
    }
}
