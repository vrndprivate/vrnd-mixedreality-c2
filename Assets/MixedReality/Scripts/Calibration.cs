﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace MR {
    public class Calibration : MonoBehaviour {
        public Text fovText;
        public Text xOffsetText;
        public Text yOffsetText;
        public Text zOffsetText;
        public Text xRotationText;
        public RawImage[] samplePreviews;
        public RenderTexture previewRT;
        public Transform wandPreviewPrefab;
        public Texture emptyTexture;
        public LayerMask previewCullingMask;

        float fovIncrement = 3;
        float offsetIncrement = 0.01f;
        float rotationIncrement = 3.0f;

        RawImage[] wandPreviews;

        void OnEnable() {
            if (wandPreviews == null) {
                CreateWandPreviews();
            } else {
                UpdateSamplePreviews();
            }
        }

        void Update() {
            if (Input.GetKeyDown(KeyCode.F)) {
                if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) {
                    Debug.Log("Incrementing FOV");
                    AdjustFieldOfView(fovIncrement);
                } else {
                    Debug.Log("Decrementing FOV");
                    AdjustFieldOfView(-fovIncrement);
                } 
            }
            if (Input.GetKeyDown(KeyCode.X)) {
                if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) {
                    Debug.Log("Incrementing X Offset");
                    AdjustOffset(new Vector3(offsetIncrement, 0, 0));
                } else {
                    Debug.Log("Decrementing X Offset");
                    AdjustOffset(new Vector3(-offsetIncrement, 0, 0));
                }
            }
            if (Input.GetKeyDown(KeyCode.Y)) {
                if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) {
                    Debug.Log("Incrementing Y Offset");
                    AdjustOffset(new Vector3(0, offsetIncrement, 0));
                } else {
                    Debug.Log("Decrementing Y Offset");
                    AdjustOffset(new Vector3(0, -offsetIncrement, 0));
                }
            }
            if (Input.GetKeyDown(KeyCode.Z)) {
                if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) {
                    Debug.Log("Incrementing Z Offset");
                    AdjustOffset(new Vector3(0, 0, offsetIncrement));
                } else {
                    Debug.Log("Decrementing Z Offset");
                    AdjustOffset(new Vector3(0, 0, -offsetIncrement));
                }
            }
            if (Input.GetKeyDown(KeyCode.R)) {
                if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)) {
                    Debug.Log("Incrementing X Rotation");
                    AdjustRotation(rotationIncrement);
                } else {
                    Debug.Log("Decrementing X Rotation");
                    AdjustRotation(-rotationIncrement);
                }
            }
            if (Input.GetKeyDown(KeyCode.Backspace)) {
                ResetAll();
            }
            fovText.text = "FOV: " + MRCamera.Instance.FOV;
            xOffsetText.text = "X Offset: " + MRCamera.Instance.PositionOffset.x;
            yOffsetText.text = "Y Offset: " + MRCamera.Instance.PositionOffset.y;
            zOffsetText.text = "Z Offset: " + MRCamera.Instance.PositionOffset.z;
            xRotationText.text = "X Rotation: " + MRCamera.Instance.XRotation;
        }

        public void AdjustFieldOfView(float increment) {
            MRCamera.Instance.FOV += increment;
            UpdateSamplePreviews();
        }

        public void AdjustOffset(Vector3 increment) {
            MRCamera.Instance.PositionOffset += increment;
            UpdateSamplePreviews();
        }

        public void AdjustRotation(float increment) {
            MRCamera.Instance.XRotation += increment;
            UpdateSamplePreviews();
        }

        public void ResetAll() {
            MRCamera.Instance.PositionOffset = Vector3.zero;
            MRCamera.Instance.XRotation = 0;
            UpdateSamplePreviews();
        }

        void CreateWandPreviews() {
            wandPreviews = new RawImage[16];
            for (int i = 0; i < 16; i++) {
                RawImage samplePreview = samplePreviews[i];
                GameObject go = new GameObject();
                go.name = "Wand Preview " + i;
                go.transform.SetParent(samplePreview.rectTransform);
                RectTransform rt = go.AddComponent<RectTransform>();
                rt.anchorMin = new Vector2(0, 0);
                rt.anchorMax = new Vector2(1, 1);
                rt.anchoredPosition3D = Vector3.zero;
                rt.offsetMin = Vector2.zero;
                rt.offsetMax = Vector2.one;
                RawImage wandPreview = go.AddComponent<RawImage>();
                wandPreview.color = Color.white;
                wandPreviews[i] = wandPreview;
            }
        }

        void UpdateSamplePreviews() {
            // Update sample snapshot textures
            List<Capture.Sample> samples = Capture.Instance.samples;
            for (int i = 0; i < 16; i++) {
                if (samples.Count > i) {
                    Capture.Sample sample = samples[i];
                    samplePreviews[i].texture = sample.tex;
                } else {
                    samplePreviews[i].texture = emptyTexture;
                }
            }
            // Synthesize wand positions
            Transform wandPreview = (Transform)Instantiate(wandPreviewPrefab, Vector3.zero, Quaternion.identity);
            wandPreview.localScale = wandPreviewPrefab.localScale;

            Camera mrCam = MRCamera.Instance.Cam;

            // Only show the wand
            LayerMask originalCullingMask = mrCam.cullingMask;
            mrCam.cullingMask = previewCullingMask;
            mrCam.clearFlags = CameraClearFlags.SolidColor;
            Transform pivot = mrCam.transform.parent;
            for (int i = 0; i < 16; i++) {
                if (samples.Count > i) {
                    // Assign the render texture and create wand previews
                    Capture.Sample sample = samples[i];
                    mrCam.targetTexture = previewRT;
                    pivot.position = sample.camPosition;
                    mrCam.transform.localPosition = MRCamera.Instance.PositionOffset;
                    pivot.rotation = sample.camRotation;
                    mrCam.transform.localRotation = Quaternion.Euler(MRCamera.Instance.XRotation, 0, 0);
                    wandPreview.position = sample.wandPosition;
                    wandPreview.rotation = sample.wandRotation;
                    wandPreview.localRotation *= Quaternion.Euler(0, 180, 0); // bugfix
                    mrCam.Render();
                    RenderTexture.active = previewRT;
                    Texture2D tex = new Texture2D(previewRT.width, previewRT.height);
                    tex.ReadPixels(new Rect(0, 0, tex.width, tex.height), 0, 0);
                    tex.Apply();                        
                    wandPreviews[i].texture = tex;
                } else {
                    wandPreviews[i].texture = emptyTexture;
                }
            }
            Destroy(wandPreview.gameObject);
            mrCam.targetTexture = null;
            // Restore camera
            mrCam.cullingMask = originalCullingMask;
            mrCam.clearFlags = CameraClearFlags.Skybox;
        }
    }
}
