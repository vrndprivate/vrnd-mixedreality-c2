﻿using UnityEngine;
using System.Collections;

namespace MR {
    public class RigRotator : MonoBehaviour {
        SteamVR_PlayArea playArea;

        void Awake() {
            playArea = FindObjectOfType<SteamVR_PlayArea>();
            if (playArea == null) {
                Debug.LogError("No SteamVR Play Area found. Please add one!");
            }
        }

        int yRot = 0;
        void Update() {
            if (Input.GetKeyDown(KeyCode.Semicolon)) {
                yRot = (yRot + 45) % 360;
                playArea.transform.rotation = Quaternion.Euler(0, yRot, 0);
            }
        }
    }
}
