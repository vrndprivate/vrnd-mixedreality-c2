﻿using UnityEngine;
using System.Collections;

namespace MR {
    public class MixedReality {
        public static MixedReality Instance {
            get {
                if (instance == null) {
                    instance = new MixedReality();
                }
                return instance;
            }
        }
        static MixedReality instance;

        public enum Mode {
            Capture,
            Calibration,
            Recording
        }
        public Mode mode;

        public SteamVR_TrackedController camController;
        public SteamVR_TrackedController wandController;
    }
}

