﻿using UnityEngine;
using System.Collections;

namespace MR {
    [RequireComponent(typeof(Camera))]
    public class MRForegroundCamera : MonoBehaviour {
        Camera cam;

        void Start() {
            cam = GetComponent<Camera>();
        }

        void LateUpdate() {
            Vector3 pos = MRCamera.Instance.Cam.transform.position;
            Quaternion rot = MRCamera.Instance.Cam.transform.rotation;
            cam.transform.position = pos;
            cam.transform.rotation = rot;
            cam.fieldOfView = MRCamera.Instance.Cam.fieldOfView;
        }
    }
}
