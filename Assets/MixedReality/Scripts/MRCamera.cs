﻿using UnityEngine;
using System.Collections;

namespace MR {
    [RequireComponent(typeof(Camera))]
    public class MRCamera : MonoBehaviour {
        public static MRCamera Instance
        {
            get { return instance; }
        }
        static MRCamera instance;

        public Camera Cam
        {
            get { return cam; }
        }
        Camera cam;

        const string mrCamFovPref = "MR.CameraFOV";
        public float FOV {
            get { return fov; }
            set
            {
                fov = value;
                PlayerPrefs.SetFloat(mrCamFovPref, fov);
                cam.fieldOfView = fov;
            }
        }
        float fov;

        const string mrCamXOffsetPref = "MR.CameraXOffset";
        const string mrCamYOffsetPref = "MR.CameraYOffset";
        const string mrCamZOffsetPref = "MR.CameraZOffset";
        public Vector3 PositionOffset
        {
            get { return positionOffset; }
            set
            {
                positionOffset = value;
                PlayerPrefs.SetFloat(mrCamXOffsetPref, positionOffset.x);
                PlayerPrefs.SetFloat(mrCamYOffsetPref, positionOffset.y);
                PlayerPrefs.SetFloat(mrCamZOffsetPref, positionOffset.z);
            }
        }
        Vector3 positionOffset;

        const string mrCamXRotationPref = "MR.CameraXRotation";
        public float XRotation
        {
            get { return xRotation; }
            set
            {
                xRotation = value;
                PlayerPrefs.SetFloat(mrCamXRotationPref, xRotation);
            }
        }
        float xRotation;

        Transform pivot;

        void Awake() {
            if (instance == null) {
                instance = this;
            } else {
                Debug.LogError("Multiple instances of MRCamera found. Please delete one!");
            }
            cam = GetComponent<Camera>();
            fov = PlayerPrefs.GetFloat(mrCamFovPref);
            if (fov > 1) {
                MRCamera.Instance.Cam.fieldOfView = fov;
            }
            positionOffset.x = PlayerPrefs.GetFloat(mrCamXOffsetPref);
            positionOffset.y = PlayerPrefs.GetFloat(mrCamYOffsetPref);
            positionOffset.z = PlayerPrefs.GetFloat(mrCamZOffsetPref);
            xRotation = PlayerPrefs.GetFloat(mrCamXRotationPref);

            pivot = transform.parent;
        }

        void LateUpdate() {
            Vector3 pos = MixedReality.Instance.camController.transform.position;
            Quaternion rot = MixedReality.Instance.camController.transform.rotation;
            pivot.position = pos;
            cam.transform.localPosition = positionOffset;
            pivot.rotation = rot;
            cam.transform.localRotation = Quaternion.Euler(xRotation, 0, 0);         
        }      
    }
}
