﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System;

namespace MR {
    public class Capture : MonoBehaviour {
        public Text numSamplesText;
        public Text deviceText;
        public RawImage liveVideoImage;
        public RawImage lastSampleImage;

        public static Capture Instance
        {
            get {
                if (instance == null) {
                    Debug.LogError("No Capture instance exists in the hierarchy! Please create one.");
                }
                return instance;
            }
        }
        static Capture instance;

        public class Sample {
            public Texture2D tex;
            public Vector3 camPosition;
            public Quaternion camRotation;
            public Vector3 wandPosition;
            public Quaternion wandRotation;
        }
        public List<Sample> samples = new List<Sample>();

        WebCamTexture webcamTex;
        int webcamDeviceIndex;
        const string webcamDeviceIndexPref = "MR.WebCamDeviceIndex";
        string deviceName;
        WebCamDevice[] devices;

        Texture emptyTexture;

        void Awake() {
            if (instance != null) {
                Debug.LogError("Multiple Capture Instances found! Please delete one in hierarchy!");
                return;
            }
            instance = this;
        }

        void OnEnable() {
            if (webcamTex != null) {
                webcamTex.Play();
            }
        }

        void OnDisable() {
            if (webcamTex != null) {
                webcamTex.Stop();
            }
        }

        void Start() {
            emptyTexture = liveVideoImage.texture; // this starts out with a placeholder image, cache it

            devices = WebCamTexture.devices;
            for (int i = 0; i < devices.Length; i++) {
                WebCamDevice device = devices[i];
            }
            webcamDeviceIndex = PlayerPrefs.GetInt(webcamDeviceIndexPref);
            SelectWebCamDevice(webcamDeviceIndex);
        }

        void Update() {
            if (Input.GetKeyDown(KeyCode.N)) {
                Debug.Log("Selecting next webcam device");
                SelectNextDevice();
            } else if (Input.GetKeyDown(KeyCode.C)) {
                Debug.Log("Capturing a sample!");
                CaptureSample();
            } else if (Input.GetKeyDown(KeyCode.Delete)) {
                Debug.Log("Deleting last sample!");
                DeleteLastSample();
            }

            deviceText.text = "DEVICE: " + devices[webcamDeviceIndex].name;
            UpdateLastSamplePreview();
            numSamplesText.text = samples.Count + "/16 SAMPLES CAPTURED";
        }

        void UpdateLastSamplePreview() {
            if (samples.Count <= 0) {
                lastSampleImage.texture = emptyTexture;
                return;
            }
            Sample lastSample = samples[samples.Count - 1];
            lastSampleImage.texture = lastSample.tex;
        }

        void SelectWebCamDevice(int index) {
            if (webcamTex != null) {
                webcamTex.Stop();
            }
            webcamTex = new WebCamTexture(devices[index].name);
            liveVideoImage.texture = webcamTex;
            webcamTex.Play();
            webcamDeviceIndex = index;
            PlayerPrefs.SetInt(webcamDeviceIndexPref, webcamDeviceIndex);
        }

        public void SelectNextDevice() {
            webcamDeviceIndex = (webcamDeviceIndex + 1) % devices.Length;
            SelectWebCamDevice(webcamDeviceIndex);
        }

        public void CaptureSample() {
            if (samples.Count >= 16) {
                Debug.LogError("No more samples can be captured.");
                return;
            }
            Sample sample = new Sample();

            Color[] pixels = webcamTex.GetPixels(0, 0, webcamTex.width, webcamTex.height);
            sample.tex = new Texture2D(webcamTex.width, webcamTex.height);
            sample.tex.SetPixels(pixels);
            sample.tex.name = "Sample " + samples.Count + 1;
            sample.tex.Apply();

            Transform cam = MixedReality.Instance.camController.transform;
            Transform wand = MixedReality.Instance.wandController.transform;
            sample.camPosition = cam.position;
            sample.camRotation = cam.rotation;
            sample.wandPosition = wand.position;
            sample.wandRotation = wand.rotation;

            samples.Add(sample);
        }

        public void DeleteLastSample() {
            if (samples.Count <= 0) {
                Debug.LogError("No samples to delete.");
                return;
            }
            samples.RemoveAt(samples.Count - 1);
        }
    }
}