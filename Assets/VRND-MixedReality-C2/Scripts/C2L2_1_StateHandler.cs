﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class C2L2_1_StateHandler : MonoBehaviour {
    public SteamVR_TrackedController leftController;
    public SteamVR_TrackedController rightController;
    [Tooltip("How much to offset chair position?")]
    public Vector3 positionOffset = Vector3.zero;
    [Tooltip("How much to offset chair rotation?")]
    public Vector3 rotationOffset = Vector3.zero;
    [Tooltip("Which chair object to create?")]
    public Transform blackChairPrefab;
    [Tooltip("Which unlit chair object to swap to?")]
    public Transform unlitChairPrefab;
    [Tooltip("Which shaded chair object to swap to?")]
    public Transform shadedChairPrefab;
    [Tooltip("Which textured chair object to swap to?")]
    public Transform texturedChairPrefab;
    [Tooltip("Which poof to show when the chair changes?")]
    public Transform poofPrefab;
    [Tooltip("Which poof to show when the chair goes away?")]
    public Transform finalPoofPrefab;

    public enum State {
        A_CreateBlackChair,
        B_UnlitChair,
        C_ShadedChair,
        D_TexturedChair,
        E_Poof,
    }
    public State state;

    SteamVR_TrackedController activeController;
    Transform chair;
    Rigidbody chairRb;

    void OnEnable() {
        leftController.TriggerClicked += TriggerClicked;
        leftController.TriggerUnclicked += TriggerUnclicked;
        leftController.Gripped += Gripped;
        rightController.TriggerClicked += TriggerClicked;
        rightController.TriggerUnclicked += TriggerUnclicked;
        rightController.Gripped += Gripped;
    }

    void OnDisable() {
        leftController.TriggerClicked -= TriggerClicked;
        leftController.TriggerUnclicked -= TriggerUnclicked;
        leftController.Gripped -= Gripped;
        rightController.TriggerClicked -= TriggerClicked;
        rightController.TriggerUnclicked -= TriggerUnclicked;
        rightController.Gripped -= Gripped;
    }

    void Update() {
        if (state == State.A_CreateBlackChair) {
            if (chair != null && activeController.triggerPressed) {
                Vector3 pos = activeController.transform.position;
                Quaternion rot = activeController.transform.rotation;
                pos += positionOffset;
                rot *= Quaternion.Euler(rotationOffset);
                chair.position = pos;
                chair.rotation = rot;
            }
        }
    }

    void TriggerClicked(object sender, ClickedEventArgs e) {
        SteamVR_TrackedController controller = (SteamVR_TrackedController)sender;
        if (state == State.A_CreateBlackChair) {
            activeController = controller;
            Vector3 pos = controller.transform.position;
            Quaternion rot = controller.transform.rotation;
            pos += positionOffset;
            rot *= Quaternion.Euler(rotationOffset);
            chair = (Transform)Object.Instantiate(blackChairPrefab, pos, Quaternion.identity);
            chair.parent = transform;
            chairRb = chair.GetComponent<Rigidbody>();
            if (chairRb == null) {
                Debug.LogError("Chair needs a rigidbody!");
                return;
            }
            chairRb.isKinematic = true;
        }
    }

    void TriggerUnclicked(object sender, ClickedEventArgs e) {
        SteamVR_TrackedController controller = (SteamVR_TrackedController)sender;
        if (state == State.A_CreateBlackChair) {
            chairRb.isKinematic = false;
        }
    }

    void Gripped(object sender, ClickedEventArgs e) {
        int numStates = System.Enum.GetNames(typeof(State)).Length;
        state = (State)(((int)state + 1) % numStates);
        Debug.Log("Gripped! " + sender + ". Switching to state: " + state);

        SteamVR_TrackedController controller = (SteamVR_TrackedController)sender;

        if (state == State.B_UnlitChair) {
            ReplaceChair(unlitChairPrefab);
        } else if (state == State.C_ShadedChair) {
            ReplaceChair(shadedChairPrefab);
        } else if (state == State.D_TexturedChair) {
            ReplaceChair(texturedChairPrefab);
        } else if (state == State.E_Poof) {
            Destroy(chair.gameObject);
            Instantiate(finalPoofPrefab, chair.transform.position, Quaternion.identity);
        }
    }

    void ReplaceChair(Transform newChairPrefab) {
        Transform newChair = (Transform)Instantiate(newChairPrefab, chair.transform.position, chair.transform.rotation);
        Destroy(chair.gameObject);
        chair = newChair;
        Instantiate(poofPrefab, chair.transform.position, Quaternion.identity);
    }

}
