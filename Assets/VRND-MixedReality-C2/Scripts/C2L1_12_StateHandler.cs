﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class C2L1_12_StateHandler : MonoBehaviour {
    [Tooltip("Which game object is the robot arm?")]
    public GameObject robotArm;
    [Tooltip("Which poof should appear when the robot becomes visible?")]
    public Transform poofPrefab;
    [Tooltip("Which material should be used to highlight sections of the robot?")]
    public Material highlightMaterial;
    [Tooltip("Bone that controls the robot arm base")]
    public Transform bottomBone;
    [Tooltip("Bone that controls the robot arm middle")]
    public Transform middleBone;
    [Tooltip("Bone that controls the robot arm top")]
    public Transform topBone;

    SteamVR_TrackedController leftController;
    SteamVR_TrackedController rightController;

    int bottomMaterialIndex = 0;
    int middleMaterialIndex = 5;
    int topMaterialIndex = 6;
    int gripperMaterialIndex = 7;

    public enum State {
        A_Start,
        B_ShowRobot,
        C_RobotIdle,
        D_RotateBottom,
        E_RotateMiddle,
        F_RotateTop,
        G_NoHighlight,
        H_RobotPlay
    }
    public State state;

    SteamVR_TrackedController[] controllers = new SteamVR_TrackedController[2];

    SimpleIKSolver simpleIk;
    GameObject dummyIkTarget;

    Vector3 lastPosition;
    SteamVR_TrackedController activeController;
    SkinnedMeshRenderer robotRenderer;

    Material previousMaterial; // holds a reference to the original (unhighlighted) material

    void OnEnable() {
        SteamVR_ControllerManager controllerManager = FindObjectOfType<SteamVR_ControllerManager>();
        leftController = controllerManager.left.GetComponent<SteamVR_TrackedController>();
        rightController = controllerManager.right.GetComponent<SteamVR_TrackedController>();

        leftController.TriggerClicked += TriggerClicked;
        leftController.TriggerUnclicked += TriggerUnclicked;
        leftController.Gripped += Gripped;
        rightController.TriggerClicked += TriggerClicked;
        rightController.TriggerUnclicked += TriggerUnclicked;
        rightController.Gripped += Gripped;
    }

    void OnDisable() {
        leftController.TriggerClicked -= TriggerClicked;
        leftController.TriggerUnclicked -= TriggerUnclicked;
        leftController.Gripped -= Gripped;
        rightController.TriggerClicked -= TriggerClicked;
        rightController.TriggerUnclicked -= TriggerUnclicked;
        rightController.Gripped -= Gripped;
    }

    void Start() {
        controllers[0] = leftController;
        controllers[1] = rightController;
        robotArm.SetActive(false);
        simpleIk = robotArm.GetComponent<SimpleIKSolver>();
        dummyIkTarget = new GameObject("Dummy IK Target");
        dummyIkTarget.transform.parent = transform;
        robotRenderer = robotArm.GetComponentInChildren<SkinnedMeshRenderer>();
    }

    void Update() {
        if (state == State.D_RotateBottom || state  == State.E_RotateMiddle || state == State.F_RotateTop) {
            Vector3 delta = lastPosition - activeController.transform.position;
            lastPosition = activeController.transform.position;
            float mag = Vector3.Magnitude(delta);
            float sign = Vector3.Dot(delta.normalized, activeController.transform.right) > 0 ? 1 : -1;
            float rotSpeed = 90.0f;

            Transform target = bottomBone;
            if (state == State.E_RotateMiddle) {
                target = middleBone;
            } else if (state == State.F_RotateTop) {
                target = topBone;
            }
            Vector3 currentEulerAngles = target.eulerAngles;
            Quaternion rot = Quaternion.Euler(currentEulerAngles.x, currentEulerAngles.y + mag * sign * rotSpeed, currentEulerAngles.z);
            target.rotation = rot;
        }
    }

    void TriggerClicked(object sender, ClickedEventArgs e) {
        SteamVR_TrackedController controller = (SteamVR_TrackedController)sender;
        
    }

    void TriggerUnclicked(object sender, ClickedEventArgs e) {
        SteamVR_TrackedController controller = (SteamVR_TrackedController)sender;
    }

    void Gripped(object sender, ClickedEventArgs e) {
        int numStates = System.Enum.GetNames(typeof(State)).Length;
        state = (State)(((int)state + 1) % numStates);
        Debug.Log("Gripped! " + sender + ". Switching to state: " + state);
        SteamVR_TrackedController controller = (SteamVR_TrackedController)sender;

        if (state == State.A_Start) {
            robotArm.SetActive(false);
        }
        if (state == State.B_ShowRobot) {
            StartCoroutine(ShowRobotArm(controller));
        } else if (state == State.C_RobotIdle) {
            StartCoroutine(PowerDownRobot(controller));
        } else if (state == State.D_RotateBottom) {
            lastPosition = controller.transform.position;
            activeController = controller;
            previousMaterial = robotRenderer.materials[bottomMaterialIndex];
            SetRobotMaterial(bottomMaterialIndex, highlightMaterial);
        } else if (state == State.E_RotateMiddle) {
            SetRobotMaterial(bottomMaterialIndex, previousMaterial);
            previousMaterial = robotRenderer.materials[middleMaterialIndex];
            SetRobotMaterial(middleMaterialIndex, highlightMaterial);
        } else if (state == State.F_RotateTop) {
            SetRobotMaterial(middleMaterialIndex, previousMaterial);
            previousMaterial = robotRenderer.materials[topMaterialIndex];
            SetRobotMaterial(topMaterialIndex, highlightMaterial);
        } else if (state == State.G_NoHighlight) {
            SetRobotMaterial(topMaterialIndex, previousMaterial);
        } else if (state == State.H_RobotPlay) {
            simpleIk.Target = controller.transform;
            simpleIk.enabled = true;
        }
    }

    void SetRobotMaterial(int materialIndex, Material mat) {
        Material[] mats = robotRenderer.materials;
        mats[materialIndex] = mat;
        robotRenderer.materials = mats;
    }

    IEnumerator ShowRobotArm(SteamVR_TrackedController controller) {
        simpleIk.Target = controller.transform;
        simpleIk.enabled = true;
        Instantiate(poofPrefab, robotArm.transform.position, poofPrefab.rotation);
        yield return new WaitForSeconds(0.5f);
        robotArm.SetActive(true);
    }

    IEnumerator PowerDownRobot(SteamVR_TrackedController controller) {
        dummyIkTarget.transform.position = controller.transform.position;
        simpleIk.Target = dummyIkTarget.transform;
        float startTime = Time.time;
        float duration = 2.0f;
        float dummyFallSpeed = 1.0f;
        while (dummyIkTarget.transform.position.y > 0 && Time.time - startTime < duration) {
            dummyIkTarget.transform.Translate(0, -dummyFallSpeed * Time.deltaTime, 0);
            yield return null;
        }
        simpleIk.enabled = false;
    }
}
