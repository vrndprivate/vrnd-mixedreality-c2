﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class C2L1_1_StateHandler : MonoBehaviour {
    [Tooltip("Which object to create using the controllers?")]
    public Transform prefabToPlace;
    [Tooltip("How hard to blow up the creations?")]
    public float explosionForce = 200.0f; 

    public enum State {
        A_PlaceObjects,
        B_RotateObjects,
        C_ScaleObjects,
        D_AddMaterials,
        E_AddLighting,
    }
    public State state;

    SteamVR_TrackedController leftController;
    SteamVR_TrackedController rightController;

    SteamVR_TrackedController[] controllers = new SteamVR_TrackedController[2];

    class Creation {
        public Transform prefabInProgress;
        public Vector3 startPosition = Vector3.zero;
    }
    Dictionary<SteamVR_TrackedController, Creation> creations = new Dictionary<SteamVR_TrackedController, Creation>();

    void OnEnable() {
        SteamVR_ControllerManager controllerManager = FindObjectOfType<SteamVR_ControllerManager>();
        leftController = controllerManager.left.GetComponent<SteamVR_TrackedController>();
        rightController = controllerManager.right.GetComponent<SteamVR_TrackedController>();

        leftController.TriggerClicked += TriggerClicked;
        leftController.TriggerUnclicked += TriggerUnclicked;
        leftController.Gripped += Gripped;
        rightController.TriggerClicked += TriggerClicked;
        rightController.TriggerUnclicked += TriggerUnclicked;
        rightController.Gripped += Gripped;
    }

    void OnDisable() {
        leftController.TriggerClicked -= TriggerClicked;
        leftController.TriggerUnclicked -= TriggerUnclicked;
        leftController.Gripped -= Gripped;
        rightController.TriggerClicked -= TriggerClicked;
        rightController.TriggerUnclicked -= TriggerUnclicked;
        rightController.Gripped -= Gripped;
    }

    void Start() {
        creations[leftController] = new Creation();
        creations[rightController] = new Creation();

        controllers[0] = leftController;
        controllers[1] = rightController;
    }

    void Update() {
        for (int i = 0; i < 2; i++) {
            SteamVR_TrackedController controller = controllers[i];
            Creation creation = creations[controller];
            Transform prefabInProgress = creation.prefabInProgress;
            if (prefabInProgress != null) {
                Vector3 pos = controller.transform.position;
                if (state == State.C_ScaleObjects) {
                    pos = creation.startPosition;
                }
                Quaternion rot = Quaternion.identity;
                if (state == State.B_RotateObjects) {
                    rot = controller.transform.rotation;
                }
                Vector3 localScale = prefabInProgress.localScale;
                if (state == State.C_ScaleObjects) {
                    float size = Vector3.Distance(creation.startPosition, controller.transform.position);
                    localScale = new Vector3(size, size, size);
                }
                prefabInProgress.position = pos;
                prefabInProgress.rotation = rot;
                prefabInProgress.localScale = localScale;
            }
        }
    }

    void TriggerClicked(object sender, ClickedEventArgs e) {
        SteamVR_TrackedController controller = (SteamVR_TrackedController)sender;
        if (state == State.A_PlaceObjects ||
            state == State.B_RotateObjects ||
            state == State.C_ScaleObjects) {
            Vector3 pos = controller.transform.position;
            Quaternion rot = Quaternion.identity;
            if (state == State.B_RotateObjects) {
                rot = controller.transform.rotation;
            }
            Creation creation = creations[controller];
            if (creation.prefabInProgress == null) {
                creation.prefabInProgress = (Transform)Instantiate(prefabToPlace, pos, rot);
                creation.prefabInProgress.parent = transform;
                creation.startPosition = pos;
                if (state == State.C_ScaleObjects) {
                    creation.prefabInProgress.localScale = new Vector3(0.01f, 0.01f, 0.01f);
                }
            } else {
                Debug.LogError("New object can't be instantiated until the trigger is released! (This shouldn't happen)");
            }
        } else if (state == State.D_AddMaterials) { 
            foreach (Transform child in transform) {
                Renderer rend = child.GetComponent<Renderer>();
                rend.material.color = Random.ColorHSV(0, 1, 0.8f, 1);
            }
        } else if (state == State.E_AddLighting) {
            SceneManager.LoadScene("VRND-C2L1-1-Part2");
        }
    }

    void TriggerUnclicked(object sender, ClickedEventArgs e) {
        SteamVR_TrackedController controller = (SteamVR_TrackedController)sender;
        if (state == State.A_PlaceObjects ||
            state == State.B_RotateObjects ||
            state == State.C_ScaleObjects) {
            // leave the object where it is and be ready to create a new one
            creations[controller].prefabInProgress = null;
        } 
    }

    void Gripped(object sender, ClickedEventArgs e) {
        int numStates = System.Enum.GetNames(typeof(State)).Length;
        state = (State)(((int)state + 1) % numStates);
        Debug.Log("Gripped! " + sender + ". Switching to state: " + state);
        if (state == State.E_AddLighting) {
            SteamVR_TrackedController controller = (SteamVR_TrackedController)sender;
            ExplodeCreations(controller.transform.position);
        }
    }

    void ExplodeCreations(Vector3 origin) {
        foreach (Transform child in transform) {
            Rigidbody rb = child.gameObject.AddComponent<Rigidbody>();
            rb.useGravity = true;
            rb.mass = 1;
            rb.AddExplosionForce(explosionForce, origin, 1.0f);
        }
    }
}
