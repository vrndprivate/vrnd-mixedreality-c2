﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class C2L5_1_StateHandler : MonoBehaviour {

    public SteamVR_TrackedObject leftController;
    public SteamVR_TrackedObject rightController;
    public GameObject spotLight;

    int counter = 0;

    public int sceneSwitchNum = 5;

    public enum State
    {
       TurnLightOn,
       TurnBakedLightsOn
    }
    private State state = State.TurnLightOn;


    // Update is called once per frame
    void Update()
    {
        SteamVR_Controller.Device leftDevice = null, rightDevice = null;

        if (leftController.gameObject.activeInHierarchy)
            leftDevice = SteamVR_Controller.Input((int)leftController.index);

        if (rightController.gameObject.activeInHierarchy)
            rightDevice = SteamVR_Controller.Input((int)rightController.index);

        if ( ( leftDevice != null && leftDevice.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger) ) || (rightDevice != null && rightDevice.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger) ) )
        {
            switch (state)
            {
                case State.TurnLightOn:
                    spotLight.gameObject.SetActive(true);
                    state++;
                    break;
                case State.TurnBakedLightsOn:
                    SceneManager.LoadScene(sceneSwitchNum);
                    state++;
                    break;
            }
           

        }
    }

}
