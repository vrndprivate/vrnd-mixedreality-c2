﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadSceneOnGrip : MonoBehaviour {
    public string sceneName;

    void OnEnable()
    {
        SteamVR_ControllerManager controllerManager = FindObjectOfType<SteamVR_ControllerManager>();
        if (controllerManager == null)
        {
            Debug.LogError("Please add the SteamVR Camera Rig prefab to the scene!");
            return;
        }
        GameObject leftControllerGo = controllerManager.left;
        GameObject rightControllerGo = controllerManager.right;
        SteamVR_TrackedController leftController = leftControllerGo.GetComponent<SteamVR_TrackedController>();
        SteamVR_TrackedController rightController = rightControllerGo.GetComponent<SteamVR_TrackedController>();
        leftController.Gripped += Gripped;
        rightController.Gripped += Gripped;
    }

    void Start()
    {
        if (sceneName == "")
        {
            Debug.LogError("LoadSceneOnGrip: You must specify a destination scene name!");
            return;
        }
    }

    void Gripped(object sender, ClickedEventArgs e)
    {
        SceneManager.LoadScene(sceneName);
    }
}
