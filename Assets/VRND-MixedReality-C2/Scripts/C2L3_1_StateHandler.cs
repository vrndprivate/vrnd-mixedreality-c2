﻿using UnityEngine;
using System.Collections;

public class C2L3_1_StateHandler : MonoBehaviour {

    public SteamVR_TrackedObject leftController;
    public SteamVR_TrackedObject rightController;
    public GameObject clockPrefab;
    public GameObject[] AnimatedCharacters;

    int counter = 0;

    public float ClockTravelSpeed = .1f;

    public enum State
    {
       PullClock,
       AnimateClock,
       SpawnCharacters
    }
    private State state = State.PullClock;

    private GameObject currClock;

    // Update is called once per frame
    void Update()
    {
        SteamVR_Controller.Device leftDevice = null, rightDevice = null;

        if (leftController.gameObject.activeInHierarchy)
            leftDevice = SteamVR_Controller.Input((int)leftController.index);

        if (rightController.gameObject.activeInHierarchy)
            rightDevice = SteamVR_Controller.Input((int)rightController.index);

        if ( leftDevice != null && leftDevice.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger)  )
        {
            switch (state)
            {
                case State.PullClock:
                    CreateClock(leftController.gameObject);
                    state++;
                    break;
                case State.AnimateClock:
                    AnimateClock();
                    state++;
                    break;
                case State.SpawnCharacters:
                    SpawnCharacter(leftController.transform.position, leftController.transform.rotation);
                    break;
            }
           

        }

        if (rightDevice != null && rightDevice.GetTouchDown(SteamVR_Controller.ButtonMask.Trigger))
        {
            switch (state)
            {
                case State.PullClock:
                    CreateClock(rightController.gameObject);
                    state++;
                    break;
                case State.AnimateClock:
                    AnimateClock();
                    state++;
                    break;
                case State.SpawnCharacters:
                    SpawnCharacter(rightController.transform.position, rightController.transform.rotation);
                    break;
            }
        }
    }

    void SpawnCharacter(Vector3 position, Quaternion rot)
    {
        GameObject temp = Instantiate(AnimatedCharacters[counter]);
        temp.transform.position = position;
        temp.transform.rotation = Quaternion.Euler(new Vector3(0f, rot.eulerAngles.y, 0f));
        counter = (counter + 1) % AnimatedCharacters.Length;
    }

    void CreateClock(GameObject controller)
    {
        currClock = Instantiate(clockPrefab);

        currClock.transform.position = controller.transform.position;
        currClock.transform.rotation = Quaternion.Euler(new Vector3(0f, controller.transform.rotation.eulerAngles.y, 0f));
    }

    void AnimateClock()
    {
        currClock.GetComponentInChildren < Animator > ().SetTrigger("RotateHands");
    }


}
